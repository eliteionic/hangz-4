import { browser } from 'protractor';
import { NoticesPageObject } from './page-objects/notices.po';

describe('Notices', () => {

	let noticesPage: NoticesPageObject;

	beforeEach(() => {

		noticesPage = new NoticesPageObject();
		noticesPage.navigateTo();

	});

	it('should display a list of notices on the page', () => {

		browser.driver.sleep(200);

		expect(noticesPage.getNoticeListItems().count()).toBeGreaterThan(0);

	});

	it('should allow the user to launch the add notice page', () => {

		noticesPage.getAddNoticeButton().click();
		browser.driver.sleep(200);

		expect(noticesPage.getModal().isDisplayed()).toBeTruthy();

	});

	it('should allow the user to close the add notice page', () => {

		noticesPage.getAddNoticeButton().click();
		browser.driver.sleep(200);

		noticesPage.getCloseButton().click();
		browser.driver.sleep(200);

		expect(noticesPage.getModal().isPresent()).toBeFalsy();

	});

	it('after adding a new notice, it should be displayed at the top of the notices list', () => {

		noticesPage.getAddNoticeButton().click();
		browser.driver.sleep(200);

		noticesPage.getTitleField().sendKeys('test title');
		noticesPage.getSaveButton().click();
		browser.driver.sleep(200);

		expect(noticesPage.getNoticeListItems().first().getText()).toContain('test title');

	});

	it('after editing a notice, the new information should be displayed in the list', () => {

		noticesPage.getAddNoticeButton().click();
		browser.driver.sleep(200);

		noticesPage.getTitleField().sendKeys('test title');
		noticesPage.getSaveButton().click();

		browser.driver.sleep(200);

		noticesPage.getEditNoticeButton().click();
		browser.driver.sleep(200);

		noticesPage.getTitleField().sendKeys('edit!');
		noticesPage.getSaveButton().click();

		browser.driver.sleep(200);

		expect(noticesPage.getNoticeListItems().first().getText()).toContain('edit!');

	});

	it('after deleting a notice, it should no longer be displayed in the list', () => {

		noticesPage.getAddNoticeButton().click();
		browser.driver.sleep(200);

		noticesPage.getTitleField().sendKeys('delete test');
		noticesPage.getSaveButton().click();

		browser.driver.sleep(200);

		let countBefore;
	
		noticesPage.getNoticeListItems().count().then((count) => {

			countBefore = count;

			let deleteNoticeButton = noticesPage.getDeleteNoticeButton();

			deleteNoticeButton.click();

			noticesPage.getConfirmDeleteNoticeButton().click();
			browser.driver.sleep(1000);

			expect(noticesPage.getNoticeListItems().count()).toBeLessThan(countBefore);		

		});

	});

});