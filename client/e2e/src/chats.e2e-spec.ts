import { element, by, browser } from 'protractor';
import { ChatPageObject } from './page-objects/chat.po';

describe('Chats', () => {

	let chatsPage: ChatPageObject;

	beforeEach(() => {

		chatsPage = new ChatPageObject();
		chatsPage.navigateTo();

	});

	it('should display a list of chats on the page', () => {

		expect(chatsPage.getChatListItems().count()).toBeGreaterThan(0);

	});

});