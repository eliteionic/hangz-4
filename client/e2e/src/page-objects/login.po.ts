import { protractor, browser, element, by, ElementFinder } from 'protractor';
import { HomePageObject } from './home.po';

export class LoginPageObject {

    homePage: HomePageObject = new HomePageObject();

    navigateTo() {
        this.homePage.navigateTo();
        this.homePage.getLogoutButton().click();
        return browser.wait(protractor.ExpectedConditions.not((protractor.ExpectedConditions.urlContains('home'))));
    }

	getLoginButton(){
		return element(by.deepCss('.login-button'));
	}

	getUsernameField(){
		return element(by.deepCss('.username-input input'));
	}

	getPasswordField(){
		return element(by.deepCss('.password-input input'));
	}
  
	getCreateAccountButton(){
		return element(by.deepCss('.create-account-button'));
	}

	getLoadingOverlay(){
		return element(by.deepCss('.loading-wrapper'));
	}

}
