import { protractor, browser, element, by, ElementFinder } from 'protractor';
import { HomePageObject } from './home.po';

export class RegisterPageObject {

    homePage: HomePageObject = new HomePageObject();

    navigateTo() {
        this.homePage.navigateTo();
        this.homePage.getLogoutButton().click();
		return browser.wait(protractor.ExpectedConditions.not((protractor.ExpectedConditions.urlContains('home'))));
    }

	getTitle(){
		return element(by.deepCss('ion-title')).getText();
	}

	getRegisterButton(){
		return element(by.deepCss('.register-button'));
	}

	getEmailField(){
		return element(by.deepCss('.register-form .email-input input'));
	}

	getUsernameField(){
		return element(by.deepCss('.register-form .username-input input'));
	}

	getPasswordField(){
		return element(by.deepCss('.register-form .password-input input'));
	}
  
	getConfirmPasswordField(){
		return element(by.deepCss('.register-form .confirm-password-input input'));
	}

}
