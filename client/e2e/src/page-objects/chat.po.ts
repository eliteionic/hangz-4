import { protractor, browser, element, by, ElementFinder } from 'protractor';
import { HomePageObject } from './home.po';

export class ChatPageObject {

    homePage: HomePageObject = new HomePageObject();

    navigateTo() {
        this.homePage.navigateTo();
        return this.homePage.getTabs().last().click();
    }

	getChatListItems(){
		return element.all(by.deepCss('.chat-list ion-item'));
	}

	getChatMessageField(){
		return element(by.deepCss('.chat-input'));
	}

	getSendChatButton(){
		return element(by.deepCss('.send-chat-button'));
	}

	getTitle(){
		return element(by.deepCss('ion-title'));
	}

}
