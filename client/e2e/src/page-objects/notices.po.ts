import { protractor, browser, element, by, ElementFinder } from 'protractor';
import { HomePageObject } from './home.po';

export class NoticesPageObject {

    homePage: HomePageObject = new HomePageObject();

    navigateTo() {
        return this.homePage.navigateTo();
    }

	getNoticeListItems(){
		return element.all(by.deepCss('.notice-list .notice-cards'));
	}
 
	getAddNoticeButton(){
		return element(by.deepCss('.add-notice-button'));
	}

	getEditNoticeButton(){
		return element(by.deepCss('.edit-notice-button'));
	}

	getDeleteNoticeButton(){
		return element(by.deepCss('.delete-notice-button'));
	}

	getConfirmDeleteNoticeButton(){
		return element(by.deepCss('.alert-button-group button'));
	}

	getSaveButton(){
		return element(by.deepCss('.save-notice-button'));
	}

	getTitleField(){
		return element(by.deepCss('.title-input input'));
	}

	getCloseButton(){
		return element(by.deepCss('ion-modal .close-modal-button'))
	}

	getModal(){
		return element(by.deepCss('ion-modal app-add-notice'));
	}

}
