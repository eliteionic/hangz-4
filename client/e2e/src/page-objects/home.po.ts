import { protractor, browser, element, by, ElementFinder } from 'protractor';

export class HomePageObject {


    navigateTo() {
        return browser.get('');
    }

	getTabs(){
		return element.all(by.deepCss('ion-tab-button'));
	}

	getSelectedTabIcon(){
		return element(by.deepCss('[aria-selected="true"] ion-icon'));
	}

	getLogoutButton(){
		return element(by.deepCss('.logout-button'));
	}

}
