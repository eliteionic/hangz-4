import { protractor, browser } from 'protractor';
import { LoginPageObject } from './page-objects/login.po';
import { RegisterPageObject } from './page-objects/register.po';

describe('Register', () => {

	let loginPage: LoginPageObject;
	let registerPage: RegisterPageObject;

	beforeEach(() => {

		loginPage = new LoginPageObject();
		registerPage = new RegisterPageObject();

		registerPage.navigateTo();

	});

	it('should redirect to the home page after successful account creation', () => {

		loginPage.getCreateAccountButton().click();

		registerPage.getUsernameField().sendKeys('testacc');

		let random = (Math.floor(Date.now() / 100000)).toString();

		registerPage.getUsernameField().sendKeys(random);
		registerPage.getEmailField().sendKeys(random + '@test.com');
		registerPage.getPasswordField().sendKeys('password');
		registerPage.getConfirmPasswordField().sendKeys('password');

		registerPage.getRegisterButton().click();

		browser.wait(protractor.ExpectedConditions.urlContains('home'));

		expect(registerPage.getTitle()).toBe('Notices');

	});

});