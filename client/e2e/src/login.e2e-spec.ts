import { protractor, browser } from 'protractor';
import { LoginPageObject } from './page-objects/login.po';
import { RegisterPageObject } from './page-objects/register.po';
import { HomePageObject } from './page-objects/home.po';

describe('Login', () => {

	let loginPage: LoginPageObject;
	let registerPage: RegisterPageObject;
	let homePage: HomePageObject;

	beforeEach(() => {

		loginPage = new LoginPageObject();
		registerPage = new RegisterPageObject();
		homePage = new HomePageObject();

		loginPage.navigateTo();

	});

	afterAll(() => {

		browser.get('');

		loginPage.getUsernameField().sendKeys('testuser');
		loginPage.getPasswordField().sendKeys('password');

		loginPage.getLoginButton().click();

		browser.wait(protractor.ExpectedConditions.urlContains('home'));

	});

	it('should be taken to the notices tab after logging in', () => {

		loginPage.getUsernameField().sendKeys('testuser');
		loginPage.getPasswordField().sendKeys('password');

		loginPage.getLoginButton().click();

		browser.wait(protractor.ExpectedConditions.urlContains('home'));

		expect(registerPage.getTitle()).toBe('Notices');

	});

	it('should be taken straight to the notices tab if already logged in', () => {

		loginPage.getUsernameField().sendKeys('testuser');
		loginPage.getPasswordField().sendKeys('password');

		loginPage.getLoginButton().click();

		browser.wait(protractor.ExpectedConditions.urlContains('home'));

		browser.get('');

		browser.wait(protractor.ExpectedConditions.urlContains('home'));

		expect(registerPage.getTitle()).toBe('Notices');

	});

	it('should be able to launch the registration page', () => {

		loginPage.getCreateAccountButton().click();

		expect(registerPage.getTitle()).toBe('Create Account');

	});

});