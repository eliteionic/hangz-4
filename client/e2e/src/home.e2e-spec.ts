import { HomePageObject } from './page-objects/home.po';

describe('Home', () => {
	
	let homePage: HomePageObject;

	beforeEach(() => {

		homePage = new HomePageObject();
		homePage.navigateTo();

	});

	it('should display two tabs', () => {
		expect(homePage.getTabs().count()).toBe(2);
	});

	it('should have the notices tab selected by default', () => {
		expect(homePage.getSelectedTabIcon().getAttribute('ng-reflect-name')).toBe('clipboard');
	});

});