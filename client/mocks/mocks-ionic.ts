import { ElementRef } from '@angular/core';

export class PlatformMock {
  public ready(): any {
    return new Promise((resolve) => {
      resolve('READY');
    });
  }

  public getQueryParam() {
    return true;
  }

  public registerBackButtonAction(fn: Function, priority?: number): Function {
    return (() => true);
  }

  public hasFocus(ele: HTMLElement): boolean {
    return true;
  }

  public doc(): HTMLDocument {
    return document;
  }

  public is(): boolean {
    return true;
  }

  public getElementComputedStyle(container: any): any {
    return {
      paddingLeft: '10',
      paddingTop: '10',
      paddingRight: '10',
      paddingBottom: '10',
    };
  }

  public onResize(callback: any) {
    return callback;
  }

  public registerListener(ele: any, eventName: string, callback: any): Function {
    return (() => true);
  }

  public win(): Window {
    return window;
  }

  public raf(callback: any): number {
    return 1;
  }

  public timeout(callback: any, timer: number): any {
    return setTimeout(callback, timer);
  }

  public cancelTimeout(id: any) {
    // do nothing
  }

  public getActiveElement(): any {
    return document['activeElement'];
  }
}

export class FormMock {
  public register(): any {
    return true;
  }
}
 
export class AppMock {

  public getRootNav(): any {
    return new NavMock();
  }

}

export class NavMock {
 
    public navigateForward(): any {
      return new Promise(function(resolve: Function): void {
        resolve();
      });
    }
   
    public navigateBackward(): any {
      return new Promise(function(resolve: Function): void {
        resolve();
      });
    }
   
    public navigateRoot(): any {
        return new Promise(function(resolve: Function): void {
          resolve();
        });
    }
  
}

export class NavParamsMock {

  public returnParams: any = {};

  public get(key): any {
    if (this.returnParams[key]) {
       return this.returnParams[key];
    }
  }

  public setParams(key, value): any {
    this.returnParams[key] = value;
  }

}

export class LoadingComponentMock {
    public present(): any {return Promise.resolve(true)}
    public dismiss(): any {return Promise.resolve(true)}
  }
  
  export class LoadingControllerMock {
    public create(): Promise<LoadingComponentMock> {
      return Promise.resolve(new LoadingComponentMock());
    }
  }

export class AlertComponentMock {
  present(): void {}
  dismiss(): void {}
}

export class AlertControllerMock {
  public create(): AlertComponentMock {
    return new AlertComponentMock();
  }
}

export class MenuMock {
  public close(): any {
    return new Promise((resolve: Function) => {
      resolve();
    });
  }
}

class ModalComponentMock {
  present(): void {}
  dismiss(): void {}
}

export class ModalControllerMock {
  public create(page): ModalComponentMock {
    return new ModalComponentMock();
  }
  present(): void {}
  dismiss(): void {}
}

export class MockElementRef extends ElementRef { 
    nativeElement = {}; 
}