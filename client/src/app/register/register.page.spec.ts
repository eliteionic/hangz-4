import { CUSTOM_ELEMENTS_SCHEMA, ElementRef } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { of, throwError } from 'rxjs';

import { ReactiveFormsModule, FormBuilder } from '@angular/forms';

import { ChatService } from '../services/chat.service';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';
import { NavController, LoadingController } from '@ionic/angular';
import { AuthServiceMock, DataServiceMock } from '../../../mocks/mocks-app';
import { NavMock, LoadingControllerMock } from '../../../mocks/mocks-ionic';
import { UserServiceMock } from '../../../mocks/mocks-app';
import { UsernameValidator } from '../validators/username';
import { EmailValidator } from '../validators/email';

import { RegisterPage } from './register.page';

let usernameValidatorStub = {
  checkUsername(control: any): any { return Promise.resolve(null); }
};

let emailValidatorStub = {
  checkEmail(control: any): any { return Promise.resolve(null); }
};

describe('RegisterPage', () => {
  let component: any;
  let fixture: ComponentFixture<RegisterPage>;
  let authProvider;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterPage ],
      imports: [ ReactiveFormsModule ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        FormBuilder,
        { provide: UsernameValidator, useValue: usernameValidatorStub },
        { provide: EmailValidator, useValue: emailValidatorStub },
        { provide: UserService, useClass: UserServiceMock },
        { provide: LoadingController, useClass: LoadingControllerMock },
        { provide: NavController, useClass: NavMock },
        { provide: AuthService, useClass: AuthServiceMock },
        { provide: DataService, useClass: DataServiceMock }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterPage);
    component = fixture.componentInstance;

    authProvider = fixture.debugElement.injector.get(AuthService);
  });

  it('should create component', () => {

    expect(component instanceof RegisterPage).toBe(true);
  
  });

  it('should have a username field', () => {
    expect(component.registerForm.contains('username')).toBe(true);
  })

  it('should have an email field', () => {
    expect(component.registerForm.contains('email')).toBe(true);
  });

  it('should have a password field', () => {
    expect(component.registerForm.contains('password')).toBe(true);
  });

  it('should have a confirm password field', () => {
    expect(component.registerForm.contains('confirmPassword')).toBe(true);
  });

  
  it('it should not allow form submission if a valid username is not present', fakeAsync(() => {

    let usernameValidator = fixture.debugElement.injector.get(UsernameValidator);
    let emailValidator = fixture.debugElement.injector.get(EmailValidator);
    spyOn(usernameValidator, 'checkUsername').and.returnValue(Promise.resolve({'usernameInUse': true}));
    spyOn(emailValidator, 'checkEmail').and.returnValue(Promise.resolve(null));
    spyOn(authProvider, 'register').and.returnValue(of(true));

    // Invalid field
    component.registerForm.controls.username.setValue('!');

    // Valid fields
    component.registerForm.controls.email.setValue('test@test.com');
    component.registerForm.controls.password.setValue('password');
    component.registerForm.controls.confirmPassword.setValue('password');

    tick(1000);

    component.createAccount();

    expect(authProvider.register).not.toHaveBeenCalled();
    
  }));

  it('it should not allow form submission if a valid email is not present', fakeAsync(() => {

    let usernameValidator = fixture.debugElement.injector.get(UsernameValidator);
    let emailValidator = fixture.debugElement.injector.get(EmailValidator);
    spyOn(usernameValidator, 'checkUsername').and.returnValue(Promise.resolve(null));
    spyOn(emailValidator, 'checkEmail').and.returnValue(Promise.resolve({'emailInUse': true}));
    spyOn(authProvider, 'register').and.returnValue(of(true));

    // Invalid field
    component.registerForm.controls.email.setValue('!');

    // Valid fields
    component.registerForm.controls.username.setValue('username');
    component.registerForm.controls.password.setValue('password');
    component.registerForm.controls.confirmPassword.setValue('password');

    tick(1000);

    component.createAccount();

    expect(authProvider.register).not.toHaveBeenCalled();

  }));

  it('it should not allow form submission if a valid password is not present', fakeAsync(() => {

    let usernameValidator = fixture.debugElement.injector.get(UsernameValidator);
    let emailValidator = fixture.debugElement.injector.get(EmailValidator);
    spyOn(usernameValidator, 'checkUsername').and.returnValue(Promise.resolve(null));
    spyOn(emailValidator, 'checkEmail').and.returnValue(Promise.resolve(null));
    spyOn(authProvider, 'register').and.returnValue(of(true));

    // Invalid field
    component.registerForm.controls.password.setValue('');

    // Valid fields
    component.registerForm.controls.username.setValue('username');
    component.registerForm.controls.email.setValue('test@test.com');
    component.registerForm.controls.confirmPassword.setValue('password');

    tick(1000);

    component.createAccount();

    expect(authProvider.register).not.toHaveBeenCalled();

  }));

  it('it should not allow form submission if the password and confirmPassword fields do not match', fakeAsync(() => {

    let usernameValidator = fixture.debugElement.injector.get(UsernameValidator);
    let emailValidator = fixture.debugElement.injector.get(EmailValidator);
    spyOn(usernameValidator, 'checkUsername').and.returnValue(Promise.resolve(null));
    spyOn(emailValidator, 'checkEmail').and.returnValue(Promise.resolve(null));
    spyOn(authProvider, 'register').and.returnValue(of(true));

    // Invalid field
    component.registerForm.controls.confirmPassword.setValue('someotherpassword');

    // Valid fields
    component.registerForm.controls.username.setValue('username');
    component.registerForm.controls.email.setValue('test@test.com');
    component.registerForm.controls.password.setValue('somepassword');
    component.registerForm.controls.confirmPassword.setValue('someotherpassword');

    tick(1000);

    component.createAccount();

    expect(authProvider.register).not.toHaveBeenCalled();

  }));

  it('it should allow the form to be submitted if form is valid', fakeAsync(() => {

    spyOn(authProvider, 'register').and.returnValue(of(true));
    spyOnProperty(component.registerForm, 'valid').and.returnValue(true);

    component.createAccount();

    tick();

    expect(authProvider.register).toHaveBeenCalled();

  }));

  it('for a successful registration, the initDatabase function on the dataprovider should be called', fakeAsync(() => {

    let fakeUserData = {
      user_id: 'test',
      token: 'test',
      password: 'test',
      issued: 'test',
      expires: 'test',
      userDBs: {
        hangz: 'test'
      } 
    };

    let dataProvider = fixture.debugElement.injector.get(DataService);
    spyOn(dataProvider, 'initDatabase');
    spyOn(authProvider, 'register').and.returnValue(of(fakeUserData));
    spyOnProperty(component.registerForm, 'valid').and.returnValue(true);

    component.createAccount();

    tick();

    expect(dataProvider.initDatabase).toHaveBeenCalledWith(fakeUserData.userDBs.hangz);

  }));

  it('for a successful registration, the saveUserData function on the userprovider should be called', fakeAsync(() => {

    let fakeUserData = {
      user_id: 'test',
      token: 'test',
      password: 'test',
      issued: 'test',
      expires: 'test',
      userDBs: {
        hangz: 'test'
      } 
    };

    let userProvider = fixture.debugElement.injector.get(UserService);

    spyOn(authProvider, 'register').and.returnValue(of(fakeUserData));
    spyOnProperty(component.registerForm, 'valid').and.returnValue(true);
    spyOn(userProvider, 'saveUserData');

    component.createAccount();

    tick();

    expect(userProvider.saveUserData).toHaveBeenCalledWith(fakeUserData);

  }));

  it('for a successful registration, the root page should be set to the Home Page', fakeAsync(() => {

    let fakeUserData = {
      user_id: 'test',
      token: 'test',
      password: 'test',
      issued: 'test',
      expires: 'test',
      userDBs: {
        hangz: 'test'
      } 
    };

    let navCtrl = fixture.debugElement.injector.get(NavController);
    spyOn(authProvider, 'register').and.returnValue(of(fakeUserData));
    spyOn(navCtrl, 'navigateRoot');
    spyOnProperty(component.registerForm, 'valid').and.returnValue(true);

    component.createAccount();

    tick();

    expect(navCtrl.navigateRoot).toHaveBeenCalledWith('/home/tabs/notices');

  }));

});
