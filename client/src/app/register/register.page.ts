import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, LoadingController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { DataService } from '../services/data.service';
import { UsernameValidator } from '../validators/username';
import { EmailValidator } from '../validators/email';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

	public registerForm: any;
	private loading: any;

	constructor(
    private navCtrl: NavController, 
    private formBuilder: FormBuilder, 
    private authService: AuthService, 
    private userService: UserService, 
    private dataService: DataService, 
    private loadingCtrl: LoadingController,
    private usernameValidator: UsernameValidator,
    private emailValidator: EmailValidator
  ) {

    this.registerForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.maxLength(16), Validators.pattern('[a-zA-Z0-9]*'), Validators.required]), usernameValidator.checkUsername.bind(usernameValidator)],
      email: ['', Validators.compose([Validators.maxLength(30), Validators.required]), emailValidator.checkEmail.bind(emailValidator)],
      password: ['', Validators.compose([Validators.maxLength(30), Validators.required])],
      confirmPassword: ['', Validators.compose([Validators.maxLength(30), Validators.required])]
    }, {validator: this.confirmPassword});

	}

	ngOnInit() {

	}

	createAccount(): void {

		if(this.registerForm.valid){

      this.loadingCtrl.create({
        message: 'Creating Account...'
      }).then((overlay) => {

        this.loading = overlay;
        this.loading.present();

        this.authService.register(this.registerForm.value).subscribe((res: any) => {

          if(typeof(res.token) != 'undefined'){
  
            this.dataService.initDatabase(res.userDBs.hangz);
            this.userService.saveUserData(res);
  
            this.navCtrl.navigateRoot('/home/tabs/notices');
  
          }
  
          this.loading.dismiss();
  
        }, (err) => {
          this.loading.dismiss();
        });

      });

		}

  }
  
	confirmPassword(form){

		let password = form.get('password');
		let confirmPassword = form.get('confirmPassword');

		let validation = {};
	
		if((password.touched || confirmPassword.touched) && password.value !== confirmPassword.value){
			validation = {
				passwordMismatch: true
			};
		}

		return validation;

	}

}