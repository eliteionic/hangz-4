import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { NoticesService } from '../services/notices.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-add-notice',
  templateUrl: './add-notice.page.html',
  styleUrls: ['./add-notice.page.scss'],
})
export class AddNoticePage implements OnInit {

  public title: string = '';
  public message: string = '';
  public existingNotice: any = false;

  constructor(
    private modalCtrl: ModalController, 
    private navParams: NavParams, 
    private noticesService: NoticesService,
    private userService: UserService
  ) { 

  }

  ngOnInit() {

    if(typeof(this.navParams.get('notice')) !== 'undefined'){
      this.existingNotice = this.navParams.get('notice');
      this.title = this.existingNotice.title;
      this.message = this.existingNotice.message;
    }

  }

  saveNotice(): void {

		if(this.title.length > 0){

			let iso = this.getDateISOString();

			this.noticesService.saveNotice({
				doc: this.existingNotice,
				title: this.title,
				message: this.message,
				author: this.userService.currentUser.user_id,
				dateCreated: iso,
				dateUpdated: iso
			});

			this.modalCtrl.dismiss();

    }
    
  }

	getDateISOString(): string {
		return new Date().toISOString();
	}

  close(): void {
    this.modalCtrl.dismiss();
  }

}