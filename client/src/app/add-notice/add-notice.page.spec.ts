import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { NoticesService } from '../services/notices.service';
import { UserService } from '../services/user.service';
import { NavParams, ModalController } from '@ionic/angular';
import { NoticesServiceMock } from '../../../mocks/mocks-app';
import { NavParamsMock, ModalControllerMock } from '../../../mocks/mocks-ionic';
import { UserServiceMock } from '../../../mocks/mocks-app';

import { AddNoticePage } from './add-notice.page';

let de: DebugElement;
let el: HTMLElement;
let input: HTMLInputElement;

describe('AddNoticePage', () => {
  let component: any;
  let fixture: ComponentFixture<AddNoticePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNoticePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: UserService, useClass: UserServiceMock },
        { provide: NoticesService, useClass: NoticesServiceMock },
        { provide: NavParams, useClass: NavParamsMock },
        { provide: ModalController, useClass: ModalControllerMock }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNoticePage);
    component = fixture.componentInstance;
  });

  afterEach(() => {

    fixture.destroy();
    component = null;
    de = null;
    el = null;
    input = null;

  });

  it('should create component', () => {

    expect(component instanceof AddNoticePage).toBe(true);
  
  });

  it('should have a title class member', () => {
    
    expect(component.title).toBeDefined();

  });

  it('should have a message class member', () => {
    
    expect(component.message).toBeDefined();

  });

  it('modifying the title input should update the title class member', () => {

    fixture.detectChanges();

    fixture.whenStable().then(() => {

      de = fixture.debugElement.query(By.css('.title-input ion-input input'));
      input = de.nativeElement;

      input.value = 'some title';
      input.dispatchEvent(new Event('input'));

      expect(component.title).toBe('some title');

    });

  });

  it('modifying the message input should update the message class member', () => {

    fixture.detectChanges();

    fixture.whenStable().then(() => {

      de = fixture.debugElement.query(By.css('.message-input input'));
      input = de.nativeElement; 
  
      input.value = 'some message';
      input.dispatchEvent(new Event('input'));

      expect(component.message).toBe('some message');

    });

  });

  it('should pass notice information to saveNotice function in notices provider when saved', () => {

    let noticesProvider = fixture.debugElement.injector.get(NoticesService);

    spyOn(noticesProvider, 'saveNotice');
    spyOn(component, 'getDateISOString').and.returnValue('new date');

    component.title = 'test title';
    component.message = 'test message';

    component.saveNotice();   

    expect(noticesProvider.saveNotice).toHaveBeenCalledWith({
      doc: false,
      title: 'test title',
      message: 'test message',
      author: 'test',
      dateCreated: 'new date',
      dateUpdated: 'new date'
    }); 

  });

  it('should not allow a notice with an empty title to be saved', () => {

    let noticesProvider = fixture.debugElement.injector.get(NoticesService);

    spyOn(noticesProvider, 'saveNotice');

    component.title = '';
    component.saveNotice();

    expect(noticesProvider.saveNotice).not.toHaveBeenCalled();

  });

  it('should dismiss the modal once the notice is saved', () => {

    let viewCtrl = fixture.debugElement.injector.get(ModalController);

    spyOn(viewCtrl, 'dismiss');

    component.title = 'some title';
    component.saveNotice();

    expect(viewCtrl.dismiss).toHaveBeenCalled();

  });

  it('should set the input fields with existing information if a notice is passed in through NavParams', () => {
  
    let navParams = fixture.debugElement.injector.get(NavParams);

    spyOn(navParams, 'get').and.returnValue({
      _id: 'someid',
      _rev: 'somerev',
      title: 'some title',
      message: 'some message'
    });

    component.ngOnInit();

    expect(component.title).toBe('some title');
    expect(component.message).toBe('some message');
  
  });

  it('if editing an existing notice, should pass information to saveNotice function when saved', () => {

    let noticesProvider = fixture.debugElement.injector.get(NoticesService);
    let navParams = fixture.debugElement.injector.get(NavParams);

    spyOn(noticesProvider, 'saveNotice');
    spyOn(component, 'getDateISOString').and.returnValue('new date');

    let doc = {
      _id: 'someid',
      _rev: 'somerev',
      title: 'original title',
      message: 'original message',
      author: 'test',
      dateCreated: 'original date',
      dateUpdated: 'original date'
    };

    spyOn(navParams, 'get').and.returnValue(doc);

    component.ngOnInit();

    component.message = 'new message';

    component.saveNotice();   

    expect(noticesProvider.saveNotice).toHaveBeenCalledWith({
      doc: doc,
      title: 'original title',
      message: 'new message',
      author: 'test',
      dateCreated: 'new date',
      dateUpdated: 'new date'
    }); 

  });

});
