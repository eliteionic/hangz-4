import { CUSTOM_ELEMENTS_SCHEMA, ElementRef } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { of, throwError } from 'rxjs';

import { FormsModule } from '@angular/forms';

import { ChatService } from '../services/chat.service';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';
import { NavController, NavParams, ModalController, LoadingController } from '@ionic/angular';
import { ChatServiceMock, AuthServiceMock, DataServiceMock } from '../../../mocks/mocks-app';
import { NavMock, NavParamsMock, ModalControllerMock, LoadingControllerMock } from '../../../mocks/mocks-ionic';
import { UserServiceMock } from '../../../mocks/mocks-app';

import { LoginPage } from './login.page';

describe('LoginPage', () => {
  let component: any;
  let fixture: ComponentFixture<LoginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginPage ],
      imports: [ FormsModule ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: UserService, useClass: UserServiceMock },
        { provide: ChatService, useClass: ChatServiceMock },
        { provide: LoadingController, useClass: LoadingControllerMock },
        { provide: NavParams, useClass: NavParamsMock },
        { provide: ModalController, useClass: ModalControllerMock },
        { provide: NavController, useClass: NavMock },
        { provide: AuthService, useClass: AuthServiceMock },
        { provide: DataService, useClass: DataServiceMock }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
  });

  it('should create component', () => {

    expect(component instanceof LoginPage).toBe(true);
  
  });

  it('should have an username class member', () => {
    
    expect(component.username).toBeDefined();

  });

  it('should have a password class member', () => {
    
    expect(component.password).toBeDefined();

  });

  it('openRegisterPage() should push the register page', () => {

    let navCtrl = fixture.debugElement.injector.get(NavController);

    spyOn(navCtrl, 'navigateForward');

    component.openRegisterPage();

    expect(navCtrl.navigateForward).toHaveBeenCalledWith('/register');

  });

  it('if an invalid response is returned from the backend, the login should fail', fakeAsync(() => {

    let dataProvider = fixture.debugElement.injector.get(DataService);
    let authProvider = fixture.debugElement.injector.get(AuthService);

    let authResponse = {
      ok: false
    };

    spyOn(dataProvider, 'initDatabase');
    spyOn(authProvider, 'authenticate').and.returnValue(throwError(authResponse));

    component.username = 'test';
    component.password = 'test';

    component.login();

    tick();

    expect(dataProvider.initDatabase).not.toHaveBeenCalled();

  }));

  it('after a successful login, the initDatabase function on the dataprovider should be called', fakeAsync(() => {

    let dataProvider = fixture.debugElement.injector.get(DataService);
    let authProvider = fixture.debugElement.injector.get(AuthService);

    let authResponse = {
      user_id: 'test',
      token: 'test',
      password: 'test',
      issued: 'test',
      expires: 'test',
      userDBs: {
        hangz: 'test'
      } 
    };

    spyOn(dataProvider, 'initDatabase');
    spyOn(authProvider, 'authenticate').and.returnValue(of(authResponse));

    component.username = 'test';
    component.password = 'test';

    component.login();

    tick();

    expect(dataProvider.initDatabase).toHaveBeenCalled();

  }));

  it('after a successful login, the user data should be passed to the userprovider', fakeAsync(() => {

    let userProvider = fixture.debugElement.injector.get(UserService);
    let authProvider = fixture.debugElement.injector.get(AuthService);

    let authResponse = {
      user_id: 'test',
      token: 'test',
      password: 'test',
      issued: 'test',
      expires: 'test',
      userDBs: {
        hangz: 'test'
      } 
    };

    spyOn(userProvider, 'saveUserData');
    spyOn(authProvider, 'authenticate').and.returnValue(of(authResponse));

    component.username = 'test';
    component.password = 'test';

    component.login();

    tick();

    expect(userProvider.saveUserData).toHaveBeenCalled();

  }));

  it('should show a loading overlay whilst an authentication request is being made', fakeAsync(() => {

    let authProvider = fixture.debugElement.injector.get(AuthService);
    let loadingCtrl = fixture.debugElement.injector.get(LoadingController);

    let spiedObject;

    spyOn(loadingCtrl, 'create').and.callFake(() => {

      spiedObject = {
        present: () => { return Promise.resolve(true) },
        dismiss: () => { return Promise.resolve(true) }
      };

      spyOn(spiedObject, 'present');

      return Promise.resolve(spiedObject);

    });

    let authResponse = {
      user_id: 'test',
      token: 'test',
      password: 'test',
      issued: 'test',
      expires: 'test',
      userDBs: {
        hangz: 'test'
      } 
    };

    spyOn(authProvider, 'authenticate').and.returnValue(of(authResponse));

    component.username = 'test';
    component.password = 'test';

    component.login();

    tick();

    expect(spiedObject.present).toHaveBeenCalled();

  }));

  it('should dimiss the loading overlay after getting a response from the server', fakeAsync(() => {

    let authProvider = fixture.debugElement.injector.get(AuthService);
    let loadingCtrl = fixture.debugElement.injector.get(LoadingController);

    let spiedObject;

    spyOn(loadingCtrl, 'create').and.callFake(() => {

      spiedObject = {
        present: () => { return Promise.resolve(true) },
        dismiss: () => { return Promise.resolve(true) }
      };

      spyOn(spiedObject, 'dismiss').and.callThrough();

      return Promise.resolve(spiedObject);

    });

    let authResponse = {
      user_id: 'test',
      token: 'test',
      password: 'test',
      issued: 'test',
      expires: 'test',
      userDBs: {
        hangz: 'test'
      } 
    };

    spyOn(authProvider, 'authenticate').and.returnValue(of(authResponse));

    component.username = 'test';
    component.password = 'test';

    component.login();

    tick();

    expect(spiedObject.dismiss).toHaveBeenCalled();

  }));

  it('after a successful login, the page should be changed to HomePage', fakeAsync(() => {

    let navCtrl = fixture.debugElement.injector.get(NavController);
    let authProvider = fixture.debugElement.injector.get(AuthService);

    let authResponse = {
      user_id: 'test',
      token: 'test',
      password: 'test',
      issued: 'test',
      expires: 'test',
      userDBs: {
        hangz: 'test'
      } 
    };

    spyOn(authProvider, 'authenticate').and.returnValue(of(authResponse));
    spyOn(navCtrl, 'navigateRoot');

    component.username = 'test';
    component.password = 'test';

    component.login();

    tick();
  
    expect(navCtrl.navigateRoot).toHaveBeenCalledWith('/home/tabs/notices');

  }));

  it('after an unsuccessful login, the failedAttempt class member should be set to true', fakeAsync(() => {

    let authProvider = fixture.debugElement.injector.get(AuthService);

    let authResponse = {
      ok: false
    };

    spyOn(authProvider, 'authenticate').and.returnValue(throwError(authResponse));

    component.username = 'test';
    component.password = 'test';

    component.login();

    tick();

    expect(component.failedAttempt).toBe(true);

  }));

  it('if the user has a valid token then they should be taken straight to the home page', fakeAsync(() => {

    let authProvider = fixture.debugElement.injector.get(AuthService);
    let navCtrl = fixture.debugElement.injector.get(NavController);

    spyOn(navCtrl, 'navigateRoot');
    spyOn(authProvider, 'authenticate').and.returnValue(Promise.resolve(true));

    component.ngOnInit();

    tick();
    
    expect(navCtrl.navigateRoot).toHaveBeenCalledWith('/home/tabs/notices');

  }));

});
