import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavController, LoadingController, IonContent, IonList } from '@ionic/angular';
import { ChatService } from '../services/chat.service';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

  @ViewChild(IonContent) contentArea: IonContent;
  @ViewChild(IonList, {read: ElementRef}) chatList: ElementRef;

  public chats: Object[] = [];
  public message: string = '';
  private loading: any;
  private mutationObserver: any;

  constructor(
		private chatService: ChatService, 
		private userService: UserService, 
		private authService: AuthService,
		private navCtrl: NavController,
		private loadingCtrl: LoadingController
	) { 

  }

  ngOnInit() {

	this.loadingCtrl.create({
		message: 'Authenticating...'
	}).then((overlay) => {

		this.loading = overlay;
		this.loading.present();

		this.authService.reauthenticate().then((res) => {

			this.loading.dismiss();

			this.chatService.init();

			this.chatService.getChats().subscribe((chats) => {

				this.chats = chats;

				if(this.chats.length === 0){

					this.chats.push({
						author: 'Hangz Admin',
						message: 'Looks like nobody is around. Type a message below to start chatting!'
					});

				}

			});

		}, (err) => {
			
			this.loading.dismiss();
			this.navCtrl.navigateRoot('/login');

		});

	});
	
	this.mutationObserver = new MutationObserver((mutations) => {
		this.newChatAdded();
	});

	this.mutationObserver.observe(this.chatList.nativeElement, {
		childList: true
	});


  }

  newChatAdded(): void {
	this.contentArea.scrollToBottom();
  }

  addChat(): void {

		if(this.message.length > 0){

			let iso = this.getDateISOString();

			this.chatService.addChat({
				message: this.message,
				author: this.userService.currentUser.user_id,
				dateCreated: iso
			});

			this.message = '';

		}    

  }

  getDateISOString(): string {
		return new Date().toISOString();
  }	

  logout(): void {
		this.authService.logout();
  }

}