import { CUSTOM_ELEMENTS_SCHEMA, ElementRef } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { of } from 'rxjs';

import { FormsModule } from '@angular/forms';

import { ChatService } from '../services/chat.service';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';
import { NavController, NavParams, ModalController, LoadingController } from '@ionic/angular';
import { ChatServiceMock, AuthServiceMock } from '../../../mocks/mocks-app';
import { NavMock, NavParamsMock, ModalControllerMock, LoadingControllerMock } from '../../../mocks/mocks-ionic';
import { UserServiceMock } from '../../../mocks/mocks-app';

import { ChatPage } from './chat.page';

let de: DebugElement;
let el: HTMLElement;
let input: HTMLInputElement;

describe('ChatPage', () => {
  let component: any;
  let fixture: ComponentFixture<ChatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatPage ],
      imports: [ FormsModule ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: UserService, useClass: UserServiceMock },
        { provide: ChatService, useClass: ChatServiceMock },
        { provide: LoadingController, useClass: LoadingControllerMock },
        { provide: NavParams, useClass: NavParamsMock },
        { provide: ModalController, useClass: ModalControllerMock },
        { provide: NavController, useClass: NavMock },
        { provide: AuthService, useClass: AuthServiceMock }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatPage);
    component = fixture.componentInstance;

    component.chatList = new ElementRef(document.createElement('ion-list'));
    component.contentArea = jasmine.createSpyObj('ion-content', ['scrollToBottom']);
  });

  afterEach(() => {

    fixture.destroy();
    component = null;
    de = null;
    input = null;

  });

  it('should create component', () => {

    expect(component instanceof ChatPage).toBe(true);
  
  });

  it('should have a message class member', () => {
    
    expect(component.message).toBeDefined();

  });

  it('should initialise the chat provider', fakeAsync(() => {

    let chatProvider = fixture.debugElement.injector.get(ChatService);

    spyOn(chatProvider, 'init');

    component.ngOnInit();
  
    tick();

    expect(chatProvider.init).toHaveBeenCalled();

  }));

  it('should have a non-empty array of chats after loading', fakeAsync(() => {

    let chatProvider = fixture.debugElement.injector.get(ChatService);
    
    chatProvider.getChats = jasmine.createSpy('getChats').and.returnValue(of([{title: 'hello'}]));    

    component.ngOnInit();

    tick();

    expect(component.chats.length).toBeGreaterThan(0);

  }));


  it('modifying the message input should update the message class member', () => {

    fixture.detectChanges();

    fixture.whenStable().then(() => {

      de = fixture.debugElement.query(By.css('ion-footer textarea'));
      input = de.nativeElement; 
  
      input.value = 'some message';
      input.dispatchEvent(new Event('input'));

      expect(component.message).toBe('some message');

    });

  });

  it('the addChat function should pass message to the chat provider', () => {

    let chatProvider = fixture.debugElement.injector.get(ChatService);

    spyOn(chatProvider, 'addChat');
    spyOn(component, 'getDateISOString').and.returnValue('new date');

    component.message = 'test message';

    component.addChat();
  
    expect(chatProvider.addChat).toHaveBeenCalledWith({
      message: 'test message', 
      author: 'test',
      dateCreated: 'new date'
    });  
  
  });

  it('should not allow a chat with an empty message to be saved', () => {

    let chatProvider = fixture.debugElement.injector.get(ChatService);

    spyOn(chatProvider, 'addChat');

    component.message = '';
    component.addChat();

    expect(chatProvider.addChat).not.toHaveBeenCalled();

  });

  it('after a new chat is added, the input field should be cleared', () => {

    component.message = 'test message';
    component.addChat();

    expect(component.message).toBe('');

  });

  it('the newChatAdded function should scroll the content to the bottom', fakeAsync(() => {

    component.newChatAdded();

    expect(component.contentArea.scrollToBottom).toHaveBeenCalled();

  }));

  it('if the user is not successfully reauthenticated, they should be kicked back to the login page', fakeAsync(() => {

    let authProvider = fixture.debugElement.injector.get(AuthService);
    let navCtrl = fixture.debugElement.injector.get(NavController);

    let authResponse = {
      ok: false
    };

    spyOn(authProvider, 'reauthenticate').and.returnValue(Promise.reject(authResponse));
    spyOn(navCtrl, 'navigateRoot');

    component.ngOnInit();

    tick();

    expect(navCtrl.navigateRoot).toHaveBeenCalledWith('/login');

  }));

  it('the logout function should call the logout method of the auth provider', () => {

    let authProvider = fixture.debugElement.injector.get(AuthService);
  
    spyOn(authProvider, 'logout');

    component.logout();

    expect(authProvider.logout).toHaveBeenCalled();

  });

});
