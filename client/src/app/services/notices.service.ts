import { Injectable, NgZone } from '@angular/core';
import { DataService } from './data.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NoticesService {

	noticesSubject: BehaviorSubject<Object[]> = new BehaviorSubject([]);

	constructor(private dataService: DataService, private zone: NgZone){

	}

	init(): void {

		this.emitNotices();

        this.dataService.db.changes({live: true, since: 'now', include_docs: true}).on('change', (change) => {

            if(change.doc.type === 'notice' || change.deleted){
                this.emitNotices();
            }

        });

	}

	getNotices(): BehaviorSubject<Object[]> {
		return this.noticesSubject;
	}

	saveNotice(notice): void {

		if(notice.doc){

			let updatedDoc = notice.doc;

			updatedDoc.title = notice.title;
			updatedDoc.message = notice.message;
			updatedDoc.dateUpdated = notice.dateUpdated;

			this.dataService.updateDoc(updatedDoc);

		} else {
			this.dataService.createDoc({
				title: notice.title,
				message: notice.message,
				author: notice.author,
				dateCreated: notice.dateCreated,
				dateUpdated: notice.dateUpdated,
				type: 'notice'
			});
		}

	}

	deleteNotice(notice): void {
		this.dataService.deleteDoc(notice);
	}

	emitNotices(): void {

		this.zone.run(() => {

			let options = {
				include_docs: true,
				descending: true
			};

			this.dataService.db.query('notices/by_date_updated', options).then((data) => {

				let notices = data.rows.map(row => {
					return row.doc;
				});

				this.noticesSubject.next(notices);

			}).catch((err) => {
				console.log(err);
			});

		});

	}

}