import { TestBed, inject, fakeAsync, tick, async } from '@angular/core/testing';
import { IonicStorageModule } from '@ionic/storage';
import { Subject } from 'rxjs';

import { DataService } from './data.service';
import { NoticesService } from './notices.service';
import { DataServiceMock } from '../../../mocks/mocks-app';

describe('AuthService', () => {

  let noticesService;
  let dataService;

  beforeEach(async(() => {

    TestBed.configureTestingModule({

      declarations: [

      ],

      providers: [
        { provide: DataService, useClass: DataServiceMock },
        NoticesService
      ],

      imports: [
        IonicStorageModule.forRoot()
      ]

    }).compileComponents();

  }));

  beforeEach(inject([NoticesService, DataService], (np, dp) => {
    noticesService = np;
    dataService = dp;
  }));

  it('getNotices should return a Subject', () => {

    expect(noticesService.getNotices() instanceof Subject);

  });

  it('emitNotices should trigger the next method on notices subject', fakeAsync(() => {

    dataService.db.query = jasmine.createSpy('query').and.returnValue(Promise.resolve({
        rows: [{value: 'test'}]
    }));

    spyOn(noticesService.noticesSubject, 'next');

    noticesService.emitNotices();

    tick();

    expect(noticesService.noticesSubject.next).toHaveBeenCalled();

  }));

  it('saveNotice should call the createDoc function in data provider if document does not already exist', () => {

    spyOn(dataService, 'createDoc');

    let testDoc = {
        doc: false,
        title: 'test title',
        message: 'test message',
        author: 'username',
        dateCreated: 'test date',
        dateUpdated: 'test date'
    };

    noticesService.saveNotice(testDoc);

    expect(dataService.createDoc).toHaveBeenCalledWith({
        title: 'test title',
        message: 'test message',
        author: 'username',
        dateCreated: 'test date',
        dateUpdated: 'test date',
        type: 'notice'
    });

  });

  it('for existing docs, saveNotice should update the doc with new values before calling updateDoc function', () => {

    spyOn(dataService, 'updateDoc');

    let testDoc = {
        doc: {
            _id: 'someid',
            _rev: 'somerev',
            title: 'oldtitle',
            message: 'oldmessage',
            author: 'oldusername',
            dateCreated: 'olddate',
            dateUpdated: 'olddate',
            type: 'notice'
        },
        title: 'new title',
        message: 'new message',
        dateUpdated: 'new date'
    };

    noticesService.saveNotice(testDoc);

    expect(dataService.updateDoc).toHaveBeenCalledWith({
        _id: 'someid',
        _rev: 'somerev',
        title: 'new title',
        message: 'new message',
        author: 'oldusername',
        dateCreated: 'olddate',
        dateUpdated: 'new date',
        type: 'notice'
    });

  });

  it('deleteNotice should pass its parameters to the deleteDoc function in the data provider', () => {

    spyOn(dataService, 'deleteDoc');

    let testNotice = {
      _id: "someid",
      _rev: "somerev",
      title: "sometitle",
      message: "somemessage"
    };

    noticesService.deleteNotice(testNotice);

    expect(dataService.deleteDoc).toHaveBeenCalledWith(testNotice);

  });

});
