import { TestBed, inject, fakeAsync, tick, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { IonicStorageModule } from '@ionic/storage';
import { of } from 'rxjs';

import { SERVER_ADDRESS } from '../../environments/environment';
import { DataService } from './data.service';
import { UserService } from './user.service';
import { AuthService } from './auth.service';
import { NavMock } from '../../../mocks/mocks-ionic';
import { UserServiceMock, DataServiceMock } from '../../../mocks/mocks-app';

describe('AuthService', () => {

  let authService;
  let dataService;
  let userService;
  let httpMock;
  let httpProvider;
  let navCtrl;

  beforeEach(async(() => {

    TestBed.configureTestingModule({

      declarations: [

      ],

      providers: [
        { provide: NavController, useClass: NavMock },
        { provide: UserService, useClass: UserServiceMock},
        { provide: DataService, useClass: DataServiceMock },
        HttpClient,
        AuthService
      ],

      imports: [
        HttpClientTestingModule,
        IonicStorageModule.forRoot()
      ]

    }).compileComponents();

  }));

  beforeEach(inject([AuthService, DataService, UserService, HttpTestingController, HttpClient, NavController], (ap, dp, up, mb, http, nav) => {
    authService = ap;
    dataService = dp;
    userService = up;
    httpMock = mb;
    httpProvider = http;
    navCtrl = nav;
  }));

  it('authenticate() should return an observable containing data from the backend', () => {

    let mockResponse = 'test';

    let testDetails = {
      username: 'test',
      password: 'test'
    };

    authService.authenticate(testDetails).subscribe(res => {
      expect(res).toBe('test');
    });    

    const mockReq = httpMock.expectOne(SERVER_ADDRESS + 'auth/login');
    mockReq.flush(mockResponse);

  });

  it('register() should return an observable containing data from the backend', () => {

    let mockResponse = 'test';

    let testDetails = {
        username: 'testing',
        email: 'test@test.com',
        password: 'password',
        confirmPassword: 'password'
    };

    authService.register(testDetails).subscribe(res => {
      expect(res).toBe('test');
    });

    const mockReq = httpMock.expectOne(SERVER_ADDRESS + 'auth/register');
    mockReq.flush(mockResponse);  

  });

  it('validate username should return an observable containing data from the backend', () => {

    let mockResponse = 'test';
    let username = 'test';

    authService.validateUsername(username).subscribe(res => {
      expect(res).toBe('test');
    });

    const mockReq = httpMock.expectOne(SERVER_ADDRESS + 'auth/validate-username/' + username);
    mockReq.flush(mockResponse);  

  });

  it('validate email should return an observable containing data from the backend', () => {

    let mockResponse = 'test';
    let email = 'test';

    authService.validateEmail(email).subscribe(res => {
      expect(res).toBe('test');
    });

    const mockReq = httpMock.expectOne(SERVER_ADDRESS + 'auth/validate-email/' + email);
    mockReq.flush(mockResponse);   

  });

  it('reauthenticate should resolve immediately if database is already set up', fakeAsync(() => {

    let resolved = false;
    let rejected = false;

    spyOn(dataService, 'db').and.returnValue(true);

    authService.reauthenticate().then((res) => {
        resolved = true;
    }, (err) => {
        rejected = true;
    });

    tick();

    expect(resolved).toBe(true);

  }));

  it('reauthenticate should return a rejected promise if no user data is available', fakeAsync(() => {

    let resolved = false;
    let rejected = false;

    dataService.db = null;
    spyOn(userService, 'getUserData').and.returnValue(Promise.resolve(null));

    authService.reauthenticate().then((res) => {
        resolved = true;
    }, (err) => {
        rejected = true;
    });

    tick();

    expect(rejected).toBe(true);

  }));

  it('reauthenticate should return a rejected promise if the expires user data is less than the current date', fakeAsync(() => {

    let resolved = false;
    let rejected = false;

    let oneWeekAgo = new Date();
    oneWeekAgo.setDate(oneWeekAgo.getDate() - 7);

    let fakeUserData = {
        user_id: 'test',
        token: 'test',
        password: 'test',
        issued: 'test',
        expires: oneWeekAgo,
        userDBs: {
            hangz: 'test'
        } 
    };

    dataService.db = null;
    spyOn(userService, 'getUserData').and.returnValue(Promise.resolve(fakeUserData));

    authService.reauthenticate().then((res) => {
        resolved = true;
    }, (err) => {
        rejected = true;
    });

    tick();

    expect(rejected).toBe(true);

  }));

  it('reauthenticate should return a resolved promise if the expires user data is greater than the current date', fakeAsync(() => {

    let resolved = false;
    let rejected = false;

    let daysLater = new Date();
    daysLater.setDate(daysLater.getDate() + 28);

    let fakeUserData = {
        user_id: 'test',
        token: 'test',
        password: 'test',
        issued: 'test',
        expires: daysLater,
        userDBs: {
            hangz: 'test'
        } 
    };

    dataService.db = null;
    spyOn(userService, 'getUserData').and.returnValue(Promise.resolve(fakeUserData));

    authService.reauthenticate().then((res) => {
        resolved = true;
    }, (err) => {
        rejected = true;
    });

    tick();

    expect(resolved).toBe(true);

  }));

  it('reauthenticate should set the currentUser in the user provider if not expired', fakeAsync(() => {

    let resolved = false;
    let rejected = false;

    let daysLater = new Date();
    daysLater.setDate(daysLater.getDate() + 28);

    let fakeUserData = {
        user_id: 'test',
        token: 'test',
        password: 'test',
        issued: 'test',
        expires: daysLater,
        userDBs: {
            hangz: 'test'
        } 
    };

    dataService.db = null;
    spyOn(userService, 'getUserData').and.returnValue(Promise.resolve(fakeUserData));

    authService.reauthenticate().then((res) => {
        resolved = true;
    }, (err) => {
        rejected = true;
    });

    tick();

    expect(userService.currentUser).toBe(fakeUserData);

  }));

  it('reauthenticate should call the initDatabase function in the data provider if not expired', fakeAsync(() => {

    let resolved = false;
    let rejected = false;

    let daysLater = new Date();
    daysLater.setDate(daysLater.getDate() + 28);

    let fakeUserData = {
        user_id: 'test',
        token: 'test',
        password: 'test',
        issued: 'test',
        expires: daysLater,
        userDBs: {
            hangz: 'test'
        } 
    };

    dataService.db = null;
    spyOn(userService, 'getUserData').and.returnValue(Promise.resolve(fakeUserData));
    spyOn(dataService, 'initDatabase');

    authService.reauthenticate().then((res) => {
        resolved = true;
    }, (err) => {
        rejected = true;
    });

    tick();

    expect(dataService.initDatabase).toHaveBeenCalledWith(fakeUserData.userDBs.hangz);

  }));

  it('the logout function should make a post request to the logout URL with authorisation headers', () => {

    let headers = new HttpHeaders();
    headers.append('Authorization', 'Bearer ' + userService.currentUser.token + ':' + userService.currentUser.password); 

    spyOn(httpProvider, 'post').and.returnValue(of(true));
    spyOn(dataService.db, 'destroy').and.returnValue(Promise.resolve(true));

    authService.logout();

    expect(httpProvider.post).toHaveBeenCalledWith(SERVER_ADDRESS + 'auth/logout', {}, {headers: headers});

  });

  it('the logout function should destroy the database in the data provider', fakeAsync(() => {

    spyOn(httpProvider, 'post').and.returnValue(of(true));
    spyOn(dataService.db, 'destroy').and.returnValue(Promise.resolve(true));

    authService.logout();

    tick();

    expect(dataService.db).toBe(null);

  }));

  it('the logout function should overwrite existing user data with null', fakeAsync(() => {

    spyOn(httpProvider, 'post').and.returnValue(of(true));
    spyOn(dataService.db, 'destroy').and.returnValue(Promise.resolve(true));
    spyOn(userService, 'saveUserData');

    authService.logout();

    tick();

    expect(userService.saveUserData).toHaveBeenCalledWith(null);

  }));

  it('the logout function should set the root page to the Login Page', fakeAsync(() => {

    spyOn(httpProvider, 'post').and.returnValue(of(true));
    spyOn(dataService.db, 'destroy').and.returnValue(Promise.resolve(true));
    spyOn(navCtrl, 'navigateRoot');

    authService.logout();

    tick();

    expect(navCtrl.navigateRoot).toHaveBeenCalledWith('/login');

  }));

});
