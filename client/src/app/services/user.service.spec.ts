import { TestBed, inject, fakeAsync, tick, async } from '@angular/core/testing';
import { IonicStorageModule } from '@ionic/storage';

import { DataService } from './data.service';
import { UserService } from './user.service';
import { DataServiceMock } from '../../../mocks/mocks-app';

describe('AuthService', () => {

  let userService;
  let dataService;

  beforeEach(async(() => {

    TestBed.configureTestingModule({

      declarations: [

      ],

      providers: [
        { provide: DataService, useClass: DataServiceMock },
        UserService
      ],

      imports: [
        IonicStorageModule.forRoot()
      ]

    }).compileComponents();

  }));

  beforeEach(inject([UserService, DataService], (up, dp) => {
    userService = up;
    dataService = dp;
  }));

  it('should have a currentUser class member', () => {

    expect(userService.currentUser).toBeDefined();

  });

  it('the saveUserData function should save user data to local storage', () => {

    let testData = {
        username: 'test'
    };

    spyOn(userService.storage, 'set');

    userService.saveUserData(testData);

    expect(userService.storage.set).toHaveBeenCalledWith('hangzUserData', testData);

  });

  it('the saveUserData function should set the currentUser class member to the user data object', () => {

    let testData = {
        username: 'test'
    };

    userService.saveUserData(testData);

    expect(userService.currentUser).toBe(testData);

  });

  it('the getUserData function should return data from local storage', () => {

    spyOn(userService.storage, 'get');

    userService.getUserData();

    expect(userService.storage.get).toHaveBeenCalledWith('hangzUserData');

  });

});
