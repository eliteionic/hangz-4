import { TestBed, inject, fakeAsync, tick, async } from '@angular/core/testing';
import { IonicStorageModule } from '@ionic/storage';
import { Subject } from 'rxjs';

import { DataService } from './data.service';
import { ChatService } from './chat.service';
import { DataServiceMock } from '../../../mocks/mocks-app';

describe('AuthService', () => {

  let chatService;
  let dataService;

  beforeEach(async(() => {

    TestBed.configureTestingModule({

      declarations: [

      ],

      providers: [
        { provide: DataService, useClass: DataServiceMock },
        ChatService
      ],

      imports: [
        IonicStorageModule.forRoot()
      ]

    }).compileComponents();

  }));

  beforeEach(inject([ChatService, DataService], (cp, dp) => {
    chatService = cp;
    dataService = dp;
  }));

  it('getChats should return a Subject', () => {

    expect(chatService.getChats() instanceof Subject);

  });

  it('emitChats should trigger the next method on chats subject', fakeAsync(() => {

    dataService.db.query = jasmine.createSpy('query').and.returnValue(Promise.resolve({
        rows: [{value: 'test'}]
    }));

    spyOn(chatService.chatsSubject, 'next');

    chatService.emitChats();

    tick();

    expect(chatService.chatsSubject.next).toHaveBeenCalled();

  }));

  it('addChat should call the createDoc function in data provider', () => {

    spyOn(dataService, 'createDoc');

    let testDoc = {
        message: 'test message',
        author: 'test',
        dateCreated: 'test date'
    };

    chatService.addChat(testDoc);

    expect(dataService.createDoc).toHaveBeenCalledWith({
        message: 'test message',
        author: 'test',
        dateCreated: 'test date',
        type: 'chat'
    });

  });

});
