import { TestBed, inject, fakeAsync, tick, async } from '@angular/core/testing';
import { IonicStorageModule } from '@ionic/storage';
import * as PouchDB from 'pouchdb/dist/pouchdb';

import { DataService } from './data.service';

describe('AuthService', () => {

  let dataService;

  beforeEach(async(() => {

    TestBed.configureTestingModule({

      declarations: [

      ],

      providers: [
        DataService
      ],

      imports: [
        IonicStorageModule.forRoot()
      ]

    }).compileComponents();

  }));

  beforeEach(inject([DataService], (dp) => {
    dataService = dp;
  }));

  it('should have access to the PouchDB object', () => {

    expect(typeof(PouchDB)).not.toBe('undefined');

  });

  it('initRemoteSync should set up remote sync', () => {
    
    dataService.initDatabase('remote');

    spyOn(dataService.db, 'sync');

    dataService.initRemoteSync();

    expect(dataService.db.sync).toHaveBeenCalled();

  });

  it('after initialising, it should have a class member that is an instance of a PouchDB database', () => {

    dataService.initDatabase('remote');
    
    expect(dataService.db instanceof PouchDB);

  });

  it('after initialising, the remote class member should be set to the supplied paramater', () => {

    dataService.initDatabase('remote');
    expect<any>(dataService.remote).toBe('remote');

  });

  it('the createDoc function should make a POST request to PouchDB with the supplied doc', () => {

    dataService.initDatabase('remote');

    spyOn(dataService.db, 'post');

    let testDoc = {
        title: 'test title',
        message: 'test message'
    };

    dataService.createDoc(testDoc);

    expect(dataService.db.post).toHaveBeenCalledWith(testDoc);

  });

  it('the updateDoc function should make a PUT request to PouchDB with the supplied doc', () => {

    dataService.initDatabase('remote');

    spyOn(dataService.db, 'put');

    let testDoc = {
        _id: 'someid',
        _rev: 'somerev',
        title: 'test title',
        message: 'test message'
    };

    dataService.updateDoc(testDoc);

    expect(dataService.db.put).toHaveBeenCalledWith(testDoc);

  });

  it('the deleteDoc function should make a remove request to PouchDB with the supplied doc', () => {

    dataService.initDatabase('remote');

    spyOn(dataService.db, 'remove');

    let testDoc = {
        _id: 'someid',
        _rev: 'somerev',
        title: 'test title',
        message: 'test message'
    };

    dataService.deleteDoc(testDoc);

    expect(dataService.db.remove).toHaveBeenCalledWith(testDoc);

  });

});
