import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { HomePage } from './home.page';

let de: DebugElement;
let el: HTMLElement;

describe('HomePage', () => {
  let component: any;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should define the first tab as the Notices Page', () => {

    de = fixture.debugElement.query(By.css('ion-tab-button:first-of-type'));

    let tabButton = de.nativeElement; 

    expect(tabButton.getAttribute('tab')).toBe('notices');

  });

  it('should define the second tab as the Chat Page', () => {

    de = fixture.debugElement.query(By.css('ion-tab-button:last-of-type'));
    let tabButton = de.nativeElement; 

    expect(tabButton.getAttribute('tab')).toBe('chat');

  });

});
