import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController, LoadingController, NavController } from '@ionic/angular';
import { AddNoticePage } from '../add-notice/add-notice.page';
import { NoticesService } from '../services/notices.service';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-notices',
  templateUrl: './notices.page.html',
  styleUrls: ['./notices.page.scss'],
})
export class NoticesPage implements OnInit {

  public notices: Object[] = [];
	public loading: any;

  constructor(
		private alertCtrl: AlertController, 
		private modalCtrl: ModalController, 
		private noticesService: NoticesService,
		private authService: AuthService,
		private loadingCtrl: LoadingController,
		private navCtrl: NavController,
		public userService: UserService
	) { 

  }

  ngOnInit() {

		this.loadingCtrl.create({
			message: 'Authenticating...'
		}).then((overlay) => {
			this.loading = overlay;
			this.loading.present();

			this.authService.reauthenticate().then((res) => {

				this.noticesService.init();
				this.loading.dismiss();
	
				this.noticesService.getNotices().subscribe((notices) => {
	
					this.notices = notices;
	
					if(this.notices.length === 0){
	
						this.notices.push({
							author: 'Hangz Admin',
							title: 'Welcome!',
							message: 'Looks like there aren\'t any notices yet. Click the \'+\' symbold to add one.'
						});
	
					}
	
				});
	
			}, (err) => {
	
				this.loading.dismiss();
				this.navCtrl.navigateRoot('/login');
	
			});

		});

  }

	openAddNoticePage(notice?): void {

		this.modalCtrl.create({
      component: AddNoticePage,
      componentProps: {
        'notice': notice
      }
		}).then((modal) => {
      modal.present();
    });

	}

	deleteNotice(notice): void {

		this.alertCtrl.create({
			header: 'Delete this notice?',
			message: 'Deleting this notice will remove it permanently.',
			buttons: [
				{
					text: 'Delete',
					handler: () => {
						notice.deleting = true;
						setTimeout(() => {
							this.noticesService.deleteNotice(notice);
						}, 500);
					}
				},
				{
					text: 'Keep it'
				}
			]
		}).then((confirm) => {
      confirm.present();
    });

	}

	logout(): void {
		this.authService.logout();
	}

}