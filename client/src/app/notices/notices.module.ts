import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NoticesPage } from './notices.page';
import { AddNoticePage } from '../add-notice/add-notice.page';
import { SkeletonCardComponent } from '../components/skeleton-card/skeleton-card.component';

const routes: Routes = [
  {
    path: '',
    component: NoticesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NoticesPage, AddNoticePage, SkeletonCardComponent],
  entryComponents: [AddNoticePage]
})
export class NoticesPageModule {}