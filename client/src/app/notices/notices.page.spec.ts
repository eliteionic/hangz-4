import { CUSTOM_ELEMENTS_SCHEMA, ElementRef } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { of, Subject } from 'rxjs';

import { FormsModule } from '@angular/forms';

import { NoticesService } from '../services/notices.service';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';
import { NavController, NavParams, ModalController, AlertController, LoadingController } from '@ionic/angular';
import { NoticesServiceMock, AuthServiceMock } from '../../../mocks/mocks-app';
import { NavMock, NavParamsMock, ModalControllerMock, AlertControllerMock, LoadingControllerMock } from '../../../mocks/mocks-ionic';
import { UserServiceMock } from '../../../mocks/mocks-app';

import { AddNoticePage } from '../add-notice/add-notice.page';
import { NoticesPage } from './notices.page';

let de: DebugElement;
let el: HTMLElement;
let input: HTMLInputElement;

describe('NoticesPage', () => {
  let component: any;
  let fixture: ComponentFixture<NoticesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoticesPage ],
      imports: [ FormsModule ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: UserService, useClass: UserServiceMock },
        { provide: NoticesService, useClass: NoticesServiceMock },
        { provide: AlertController, useClass: AlertControllerMock },
        { provide: LoadingController, useClass: LoadingControllerMock },
        { provide: NavParams, useClass: NavParamsMock },
        { provide: ModalController, useClass: ModalControllerMock },
        { provide: NavController, useClass: NavMock },
        { provide: AuthService, useClass: AuthServiceMock }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticesPage);
    component = fixture.componentInstance;
  });

  afterEach(() => {

    fixture.destroy();
    component = null;
    de = null;
    input = null;

  });

  it('should create component', () => {

    expect(component instanceof NoticesPage).toBe(true);
  
  });

  it('should initialise the notices provider', fakeAsync(() => {

    let noticesProvider = fixture.debugElement.injector.get(NoticesService);

    spyOn(noticesProvider, 'init');

    component.ngOnInit();

    tick();

    expect(noticesProvider.init).toHaveBeenCalled();

  }));

  it('should have a non-empty array of notices after loading', fakeAsync(() => {

    let noticesProvider = fixture.debugElement.injector.get(NoticesService);
    
    noticesProvider.getNotices = jasmine.createSpy('getNotices').and.returnValue(of([{title: 'hello'}]));    

    component.ngOnInit();

    tick();

    expect(component.notices.length).toBeGreaterThan(0);

  }));

  it('should update notices member whenever a new notice is added by the notices provider', fakeAsync(() => {

    let noticesProvider = fixture.debugElement.injector.get(NoticesService);
    let fakeNoticesSubject = new Subject();

    spyOn(component, 'notices');
    spyOn(noticesProvider, 'getNotices').and.returnValue(fakeNoticesSubject);

    component.ngOnInit();

    tick();

    expect(component.notices.length).toBe(0);

    fakeNoticesSubject.next([{title: 'test', message: 'test', author: 'test'}]);

    tick();

    expect(component.notices.length).toBe(1);

  }));

  it('should pass the notice to the add notice modal when editing a notice', () => {

    let modalCtrl = fixture.debugElement.injector.get(ModalController);
    let spiedObject;

    spyOn(modalCtrl, 'create').and.callFake(() => {
      spiedObject = {
        present: () => {}
      };

      spyOn(spiedObject, 'present');

      return Promise.resolve(spiedObject);
    });

    let testNotice = {
      _id: "someid",
      _rev: "somerev",
      title: "sometitle",
      message: "somemessage"
    }

    let modalOptions = {
      component: AddNoticePage,
      componentProps: {
        'notice': testNotice
      }
    }

    component.openAddNoticePage(testNotice);

    expect(modalCtrl.create).toHaveBeenCalledWith(modalOptions);

  });

  it('openAddNoticePage should create a modal for the add notice page', () => {

    let modalCtrl = fixture.debugElement.injector.get(ModalController);

    let modalOptions = {
      component: AddNoticePage,
      componentProps: {
        'notice': undefined
      }
    }

    let spiedObject;

    spyOn(modalCtrl, 'create').and.callFake(() => {
      spiedObject = {
        present: () => {}
      };

      spyOn(spiedObject, 'present');

      return Promise.resolve(spiedObject);
    });

    component.openAddNoticePage();
    
    expect(modalCtrl.create).toHaveBeenCalledWith(modalOptions);

  });

  it('openAddNoticePage should call the present function on the modal', fakeAsync(() => {

    let modalCtrl = fixture.debugElement.injector.get(ModalController);

    let spiedObject;

    spyOn(modalCtrl, 'create').and.callFake(() => {

      spiedObject = {
        present: () => {}
      };

      spyOn(spiedObject, 'present');

      return Promise.resolve(spiedObject);

    });

    component.openAddNoticePage();

    tick();

    expect(spiedObject.present).toHaveBeenCalled();

  }));

  it('the delete notice function should present a confirmation overlay', fakeAsync(() => {

    let alertCtrl = fixture.debugElement.injector.get(AlertController);

    let spiedObject;

    spyOn(alertCtrl, 'create').and.callFake(() => {

      spiedObject = {
        present: () => {}
      };

      spyOn(spiedObject, 'present');

      return Promise.resolve(spiedObject);

    });

    let testNotice = {
      _id: "someid",
      _rev: "somerev",
      title: "sometitle",
      message: "somemessage"
    };

    component.deleteNotice(testNotice);

    tick();

    expect(spiedObject.present).toHaveBeenCalled();
  
  }));

  it('if the user is not successfully reauthenticated, they should be kicked back to the login page', fakeAsync(() => {

    let authProvider = fixture.debugElement.injector.get(AuthService);
    let navCtrl = fixture.debugElement.injector.get(NavController);

    let authResponse = {
      ok: false
    };

    spyOn(authProvider, 'reauthenticate').and.returnValue(Promise.reject(authResponse));
    spyOn(navCtrl, 'navigateRoot');

    component.ngOnInit();

    tick();

    expect(navCtrl.navigateRoot).toHaveBeenCalledWith('/login');

  }));

  it('the logout function should call the logout method of the auth provider', () => {

    let authProvider = fixture.debugElement.injector.get(AuthService);
  
    spyOn(authProvider, 'logout');

    component.logout();

    expect(authProvider.logout).toHaveBeenCalled();

  });

});
