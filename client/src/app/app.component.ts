import { Component, NgZone } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { DataService } from './services/data.service';

const { SplashScreen, StatusBar } = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private dataService: DataService,
    private zone: NgZone
  ) {
    this.initializeApp();
  }

  initializeApp() {

    SplashScreen.hide().catch((err) => {
      console.warn(err);
    });

    StatusBar.hide().catch((err) => {
      console.warn(err);
    });

  }
}